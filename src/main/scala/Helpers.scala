import java.math.MathContext

/**
  * Created by rsladek on 20.04.2016.
  */


object Helpers {

  def convertListOfStringsToListOfInts(input: List[String]): List[Int] = input.map(_.toInt)

  def getSumOfAList(input: List[Int]): Int = input.sum

  def getNElementsFromInputList(input: List[String])(numOfLines: Int): List[String] = input.take(numOfLines)

  def factorialRecursive(num: Int): Long = num match {
    case 0 => 1
    case _ => num * factorialRecursive(num - 1)
  }

  def factorialFP(num: Int): Long = 1L.to(num).reduceLeft((x, y) => x * y)

  def generatePointsWithinLimitByInterval(lowerLimit: Double, upperLimit: Double, interval: Double) = (lowerLimit to upperLimit by interval).toList.map(doubleToBD32(_, 3))

  def doubleToBD32(value: Double, precision: Int = 3) = BigDecimal(value, MathContext.DECIMAL32).setScale(precision, BigDecimal.RoundingMode.HALF_EVEN)

  def distanceBetween(point_a: (Int, Int), point_b: (Int, Int)): Double = {
    val a = point_b._1 - point_a._1
    val b = point_b._2 - point_a._2
    math.sqrt(a * a + b * b)
  }
}