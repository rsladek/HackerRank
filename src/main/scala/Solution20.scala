
/**
  * Created by rsladek on 23.04.2016.
  */
object Solution20 {
  def pascalsTriangle(k: Int): List[List[Int]] = {
    val rows = 0.to(k - 1)
    for (row <- rows) yield printRow(row)
  }.toList

  def printRow(row: Int): List[Int] = {
    val columns = 0.to(row)
    val rowValues = {
      for (col <- columns) yield columnValue(row, col)
    }.toList
    println(rowValues.mkString(" "))
    rowValues
  }

  def columnValue(row: Int, col: Int): Int = factorial(row) / (factorial(col) * factorial(row - col))

  def factorial(n: Int): Int = {
    n match {
      case 0 => 1
      case i: Int => i * factorial(i - 1)
    }
  }

  def main(args: Array[String]) {
    /** This will handle the input and output **/
    pascalsTriangle(readInt())
  }
}
