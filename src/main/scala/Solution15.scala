//https://www.hackerrank.com/challenges/lambda-march-compute-the-area-of-a-polygon

object Solution15 {

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines
    val numberOfLines = lines.take(1).map(_.trim).map(_.toInt).sum
    val input: List[(Int, Int)] = lines.take(numberOfLines).map(_.split(" ").take(2) match {
      case Array(x, y) => (x.trim.toInt, y.trim.toInt)
      case _ => null
    }).toList

    println(areaOfPolygon(input))
  }

  def areaOfPolygon(arr: List[(Int, Int)]): Double = {
    // http://www.mathopenref.com/coordpolygonarea.html
    def calculateArea(points: List[(Int, Int)]): Double = points match {
      case (first :: second :: Nil) => {
        area(first, second)
      }
      case (first :: second :: tail) => {
        calculateArea(second :: tail) + area(first, second)
      }
      case _ => 0.0
    }
    def area(point_a: (Int, Int), point_b: (Int, Int)): Double = {
      val x1y2 = point_a._1 * point_b._2
      val x2y1 = point_b._1 * point_a._2
      x1y2 - x2y1
    }
    calculateArea(arr :+ arr(0)).abs / 2
  }
}

