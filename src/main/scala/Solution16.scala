/**
  * Created by rsladek on 23.04.2016.
  */

// https://www.hackerrank.com/challenges/functions-or-not
object Solution16 {

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines
    val numberOfTests = lines.take(1).map(_.trim).map(_.toInt).sum
    println("Num of tests: " + numberOfTests)
    val tests = for (numTest <- 1 to numberOfTests) yield {
      val numberOfPointsInTest = lines.take(1).map(_.trim).map(_.toInt).sum
      println("Num of points in test: " + numberOfPointsInTest)
      val testLines = lines.take(numberOfPointsInTest).toList
      testLines.foreach(println)
      val points = for (line <- testLines) yield {
        println("Line: " + line)
        val pair = line.split(" ").take(2)
        val parsedPair = pair match {
          case Array(x, y) => Some(x.trim.toInt, y.trim.toInt)
          case _ => None
        }
        println("ParsedPair: " + parsedPair)
        parsedPair
      }
      points
    }
    tests.foreach(t => isAFunction(t))

  }

  def isAFunction(points: Seq[Option[(Int, Int)]]): Unit = {
    val hasNoEmptyPairs: Boolean = points.forall(_.isDefined)

    val hasNoDuplicatesX: Boolean = points.flatten.foldLeft((Set.empty[Int], Set.empty[Int])) {
      case ((seen, duplicates), cur ) =>
        if (seen(cur._1)) (seen, duplicates + cur._1) else (seen + cur._1, duplicates)
    }._2.isEmpty
    hasNoEmptyPairs && hasNoDuplicatesX match {
      case true => println("YES")
      case _ => println("NO")
    }
  }
}
