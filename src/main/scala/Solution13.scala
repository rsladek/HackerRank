/**
  * Created by rsladek on 23.04.2016.
  */

// https://www.hackerrank.com/challenges/area-under-curves-and-volume-of-revolving-a-curv

object Solution13 {
  // This function will be used while invoking "Summation" to compute
  // The area under the curve.
  def f(coefficients: List[Int], powers: List[Int], xn: Double): Double = {
    for ((c, p) <- coefficients.zip(powers)) yield c * math.pow(xn, p)
  }.sum

  // This function will be used while invoking "Summation" to compute
  // The Volume of revolution of the curve around the X-Axis
  // The 'Area' referred to here is the area of the circle obtained
  // By rotating the point on the curve (x,f(x)) around the X-Axis
  def area(coefficients: List[Int], powers: List[Int], x: Double): Double = math.Pi * math.pow(f(coefficients, powers, x), 2)

  // This is the part where the series is summed up
  // This function is invoked once with func = f to compute the area         // under the curve
  // Then it is invoked again with func = area to compute the volume
  // of revolution of the curve
  def summation(func: (List[Int], List[Int], Double) => Double, upperLimit: Int, lowerLimit: Int, coefficients: List[Int], powers: List[Int]): Double = {
    def squerMethod = (fx: IndexedSeq[Double], dx: Double) => fx.map(_ * dx).sum
    val dx = 1 / 1000.toDouble
    val fxn = Range.Double(lowerLimit, upperLimit - dx, dx).map(func(coefficients, powers, _))
    val result = squerMethod(fxn, dx)
    println(result)
    result
  }
}
