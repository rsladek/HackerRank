/**
  * Created by rsladek on 23.04.2016.
  */
object Solution17 {

  def main(args: Array[String]) {

    val lines = io.Source.stdin.getLines
    val numberOfStrings = lines.take(1).map(_.trim).map(_.toInt).sum
    println("Num of strings: " + numberOfStrings)
    val strings: List[String] = lines.take(numberOfStrings).map(_.trim).toList

    for (string <- strings) yield {
      rotationOfString(string)
    }
  }

  def rotationOfString(input: String): List[String] = {
    var currentWord = input.toList
    val result: List[String] = for (character <- currentWord) yield {
      currentWord match {
        case head :: tail if head == character => {
          currentWord = tail :+ head;
          printAWord(currentWord)
        }
      }
    }
    println
    result
  }

  def printAWord(word: List[Char]): String = {
    val s = word.mkString
    print(s + " ")
    s
  }
}
