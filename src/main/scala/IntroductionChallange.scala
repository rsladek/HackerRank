import Helpers._

/**
  * Created by rsladek on 20.04.2016.
  */

object IntroductionChallenge {

  //1. https://www.hackerrank.com/challenges/fp-solve-me-first
  def solveMeFirst() = {
    def compositeFunction = getNElementsFromInputList(io.Source.stdin.getLines.toList) _ andThen convertListOfStringsToListOfInts _ andThen getSumOfAList _
    compositeFunction(2)
  }

  //2. https://www.hackerrank.com/challenges/fp-hello-world
  def helloWorld() = println("Hello World")

  //3. https://www.hackerrank.com/challenges/fp-hello-world-n-times
  def helloWorldNTimes(n: Int) = for (i <- 1 to n) println("Hello World")

  //4. https://www.hackerrank.com/challenges/fp-list-replication
  def listReplication(num: Int, arr: List[Int]): List[Int] = {
    for (item <- arr) yield List.fill(num)(item)
  }.flatten

  //5. https://www.hackerrank.com/challenges/fp-filter-array
  def filterArray(delim: Int, arr: List[Int]): List[Int] = for (item <- arr if item < delim) yield item

  //6. https://www.hackerrank.com/challenges/fp-filter-positions-in-a-list
  def filterPositionsInList(arr: List[Int]): List[Int] = {
    for (item <- arr.zipWithIndex if ((item._2) % 2 == 1)) yield item._1
  }

  //7. https://www.hackerrank.com/challenges/fp-array-of-n-elements
  def arrayOfNElements(num: Int): List[Int] = List.range(1, num + 1)

  //8.https://www.hackerrank.com/challenges/fp-reverse-a-list
  def reverseList(arr: List[Int]): List[Int] = {
    def reverse[A](result: List[A], list: List[A]): List[A] = {
      list match {
        case Nil => result
        case (x :: xs) => {
          reverse(x :: result, xs)
        }
      }
    }
    reverse(Nil, arr)
  }

  //9. https://www.hackerrank.com/challenges/fp-sum-of-odd-elements
  def sumOfOddElements(arr: List[Int]): Int = {
    for (i <- arr if i.abs % 2 == 1) yield i
  }.sum

  //10. https://www.hackerrank.com/challenges/fp-list-length
  def listLength(arr: List[Int]): Int = {
    for (i <- arr) yield 1
  }.sum

  //11. https://www.hackerrank.com/challenges/fp-update-list
  def updateList(arr: List[Int]): List[Int] = arr.map(_.abs)

  //12. https://www.hackerrank.com/challenges/eval-ex
  def evalEx(x: Float): Float = {
    def term(num: Float, order: Int) = (Math.pow(num, order) / Helpers.factorialRecursive(order)) * 10000.toLong
    0.to(9).map(term(x, _)).sum.round / 10000.toFloat
  }

  //for the rest of the challenges pls have a look into solution files.

}