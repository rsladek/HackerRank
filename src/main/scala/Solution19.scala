/**
  * Created by rsladek on 23.04.2016.
  */

// https://www.hackerrank.com/challenges/functional-programming-warmups-in-recursion---gcd
object Solution19 {

  def gcd(x: Int, y: Int): Int = {
    val pair = List(x, y)
    pair.max % pair.min match {
      case 0 => pair.min
      case r: Int => gcd(pair.min, r)
    }
  }

  /** This part handles the input/output. Do not change or modify it **/
  def acceptInputAndComputeGCD(pair: List[Int]) = {
    println(gcd(pair.head, pair.reverse.head))
  }

  def main(args: Array[String]) {
    /** The part relates to the input/output. Do not change or modify it **/
    acceptInputAndComputeGCD(readLine().trim().split(" ").map(x => x.toInt).toList)

  }
}
