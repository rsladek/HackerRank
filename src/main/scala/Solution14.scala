/**
  * Created by rsladek on 23.04.2016.
  */

// https://www.hackerrank.com/challenges/lambda-march-compute-the-perimeter-of-a-polygon

object Solution14 {

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines
    val numberOfLines = lines.take(1).map(_.trim).map(_.toInt).sum
    val input: List[(Int, Int)] = lines.take(numberOfLines).map(_.split(" ").take(2) match {
      case Array(x, y) => (x.trim.toInt, y.trim.toInt)
      case _ => null
    }).toList

    println(perimeterOfPolygon(input))
  }

  def perimeterOfPolygon(arr: List[(Int, Int)]): Double = {
    def calculatePerimeter(points: List[(Int, Int)]): Double = points match {
      case (first :: second :: Nil) => {
        distanceBetween(first, second)
      }
      case (first :: second :: tail) => {
        calculatePerimeter(second :: tail) + distanceBetween(first, second)
      }
      case _ => 0.0
    }
    def distanceBetween(point_a: (Int, Int), point_b: (Int, Int)): Double = {
      val a = point_b._1 - point_a._1
      val b = point_b._2 - point_a._2
      math.sqrt(a * a + b * b)
    }
    Helpers.doubleToBD32(calculatePerimeter(arr :+ arr.head), 7).toDouble
  }
}