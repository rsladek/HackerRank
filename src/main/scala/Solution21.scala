/**
  * Created by rsladek on 23.04.2016.
  */
object Solution21 {

  def mingling(arr: List[String]): String = {
    val first = arr.head
    val last = arr.reverse.head
    help(first.toList, last.toList).mkString
  }

  def help(word1: List[Char], word2: List[Char]): List[Char] = {
    (word1: List[Char], word2: List[Char]) match {
      case (a :: Nil, b :: Nil) => List(a, b)
      case (a :: tailA, b :: tailB) => List(a, b) ::: help(tailA, tailB)
    }
  }

  def main(args: Array[String]) {
    val lines = io.Source.stdin.getLines
    println(mingling(lines.take(2).map(_.trim).toList))
  }
}
