/**
  * Created by rsladek on 23.04.2016.
  */
object Solution18 {

  def fibonacci(n: Int): Int = {
    n match {
      case 1 => 0
      case 2 => 1
      case t: Int if (t > 2) => fibonacci(t - 2) + fibonacci(t - 1)
    }
  }

  def main(args: Array[String]) {
    /** This will handle the input and output **/
    println(fibonacci(readInt()))

  }
}
