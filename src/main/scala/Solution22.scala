/**
  * Created by rsladek on 23.04.2016.
  */
object Solution22 {
  def main(args: Array[String]) {
    val sc = new java.util.Scanner(System.in);
    val n = sc.nextInt();
    val a = Array.ofDim[Int](n, n);
    for (a_i <- 0 to n - 1) {
      for (a_j <- 0 to n - 1) {
        a(a_i)(a_j) = sc.nextInt();
      }
    }
    println(diagonalDifference  (a, n))
  }

  def diagonalDifference(a: Array[Array[Int]], n: Int): Int = {
    printMatrix(a, n)
    sumDiagonal(a, n)
    val b = revertMatrix(a, n)
    println()
    printMatrix(b, n)
    println(sumDiagonal(b, n))
    (sumDiagonal(a, n) - sumDiagonal(b, n)).abs
  }

  def sumDiagonal(a: Array[Array[Int]], n: Int): Int = {
    val diagonals = for {
      i <- 0 until n
    } yield a(i)(i)
    diagonals.sum
  }

  def printMatrix(a: Array[Array[Int]], n: Int): Unit = {
    for {
      i <- 0 until n
      j <- 0 until n
    } println(s"($i)($j) = ${a(i)(j)}")
  }

  def revertMatrix(a: Array[Array[Int]], n: Int) = {
    val result = for {
      i <- 0 until n
    } yield a(i).reverse
    result.toArray
  }
}
