import Solution22._
import org.scalatest.{BeforeAndAfterEach, FlatSpec, Matchers}

/**
  * Created by rsladek on 23.04.2016.
  */
class Solution22$Test extends FlatSpec with BeforeAndAfterEach with Matchers {
  val n = 3
  val matrixA = Array.ofDim[Int](n, n)
  val matrixB = Array.ofDim[Int](n, n)

  override def beforeEach() {
    matrixA(0)(0) = 11
    matrixA(0)(1) = 2
    matrixA(0)(2) = 4
    matrixA(1)(0) = 4
    matrixA(1)(1) = 5
    matrixA(1)(2) = 6
    matrixA(2)(0) = 10
    matrixA(2)(1) = 8
    matrixA(2)(2) = -12

    matrixB(0)(2) = 11
    matrixB(0)(1) = 2
    matrixB(0)(0) = 4
    matrixB(1)(2) = 4
    matrixB(1)(1) = 5
    matrixB(1)(0) = 6
    matrixB(2)(2) = 10
    matrixB(2)(1) = 8
    matrixB(2)(0) = -12
  }

  behavior of "Diagonal Difference"

  it should "sumDiagonal" in {
    sumDiagonal(matrixA, n) should be(4)
  }

  it should "diagonalDifferenc" in {
    diagonalDifference(matrixA, n) should be(15)
  }

  it should "revertMatrix" in {
    revertMatrix(matrixA, n) should be(matrixB)
  }

}
