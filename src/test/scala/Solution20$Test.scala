import Solution20._
import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by rsladek on 23.04.2016.
  */
class Solution20$Test extends FlatSpec with Matchers {

  behavior of "Solution20$Test"

  it should "factorial" in {
    factorial(5) should be(120)
  }

  it should "printRow" in {
    printRow(2) should be(List(1, 2, 1))
  }

  it should "columnValue" in {
    columnValue(row = 4, col = 3) should be(4)
  }

  it should "pascalsTriangle" in {
    pascalsTriangle(3) should be(List(List(1), List(1, 1), List(1, 2, 1)))
  }

}
