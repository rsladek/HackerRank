import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by rsladek on 23.04.2016.
  */
class Solution19$Test extends FlatSpec with Matchers {

  behavior of "greatest-common-divisor"

  it should "return 5" in {
    Solution19.gcd(45, 10) should be(5)
  }
  it should "return 57" in {
    Solution19.gcd(155667, 54321) should be(57)
  }
}
