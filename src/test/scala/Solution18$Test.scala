import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by rsladek on 23.04.2016.
  */
class Solution18$Test extends FlatSpec with Matchers {

  behavior of "fibonacci"

  it should "return 0" in {
    Solution18.fibonacci(1) should be(0)
  }
  it should "return 1" in {
    Solution18.fibonacci(2) should be(1)
    Solution18.fibonacci(3) should be(1)
  }
  it should "return 2" in {
    Solution18.fibonacci(4) should be(2)
  }

}
