import java.io.ByteArrayInputStream

import IntroductionChallenge._
import org.scalatest.{FlatSpec, Matchers}

class IntroductionChallenge$Test extends FlatSpec with Matchers {

  "1. solveMeFirst" should "return sum of two numbers" in {
    System.setIn(new ByteArrayInputStream("2\r\n5".getBytes))
    solveMeFirst() should be(7)
  }

  "4. listReplication" should "return list with n times elements from input list" in {
    listReplication(3, List(3, 7, 4)) should be(List(3, 3, 3, 7, 7, 7, 4, 4, 4))
  }

  "5. filterArray" should "return list with elements smaller than 3" in {
    filterArray(3, List(10, 9, 8, 2, 7, 5, 1, 3, 0)) should be(List(2, 1, 0))
  }

  "6. filterPositionsInList" should "return list with odds elements only" in {
    filterPositionsInList(List(2, 5, 3, 4, 6, 7, 9, 8)) should be(List(5, 4, 7, 8))
  }

  "7. arrayOfNElements" should "return list from 1 to 10" in {
    arrayOfNElements(10) should be(List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10))
  }

  "8. reverseList" should "return revers list" in {
    reverseList(List(19, 22, 3, 28, 26, 17, 18, 4, 28, 0)) should be(List(0, 28, 4, 18, 17, 26, 28, 3, 22, 19))
  }

  "9. sumOfOddElements" should "return sum of odd elements" in {
    sumOfOddElements(List(3, 2, 4, 6, 5, 7, 8, 0, 1)) should be(16)
    sumOfOddElements(List(-17, -8, 2, -4, 20, 25, 0, -21, -29, 9, 9, 3, 16, 2, -29, -27, 17, 25, -7, 7, -18, -27, 18, -4, -4, 28, 11, -21, -17, 27)) should be(-62)
  }

  "10. listLength" should "return number of elements" in {
    listLength(List(2, 5, 1, 4, 3, 7, 8, 6, 0, 9)) should be(10)
  }

  "11. updateList" should "return list with absolut values" in {
    updateList(List(2, -4, 3, -1, 23, -4, -54)) should be(List(2, 4, 3, 1, 23, 4, 54))
  }

  "12. evalEx" should "return exp to x" in {
    evalEx(5.0000f) should be(143.6895f)
    evalEx(0.5000f) should be(1.6487f)
    evalEx(-0.5000f) should be(0.6065f)
    evalEx(20.0000f) should be(2423600.1887f)
  }
}
