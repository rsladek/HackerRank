import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by rsladek on 23.04.2016.
  */
class Solution13$Test extends FlatSpec with Matchers {

  behavior of "Solution13$Test"

  "solution 13 fxn" should "return value 4 of the f(x) = x^2" in {
    Solution13.f(List(0, 1), List(1, 2), xn = 2) should be(4.0)
  }

  "solution 13 f(x)=2x" should "return value y=0 for x=0" in {
    Solution13.f(List(2), List(1), 0) should be(0)
  }

  "solution 13 f(x)=2x^2" should "return value y=4 for x=1" in {
    Solution13.f(List(2), List(2), 1) should be(2)
  }

  "solution 13 f(x)=3" should "return value y=3 for x=1" in {
    Solution13.f(List(3), List(0), 1) should be(3)
  }

  "solution 13 f(x)=-5" should "return value y=-5 for x=1" in {
    Solution13.f(List(-5), List(0), 3) should be(-5)
  }

  "solution 13 f(x)=3+5x+3x^2" should "return value y=3 for x=0" in {
    Solution13.f(List(3, 5, 3), List(0, 1, 2), 0) should be(3)
  }

  "solution 13 f(x)=-3-5x-3x^2" should "return value y=-3 for x=0" in {
    Solution13.f(List(-3, -5, -3), List(0, 1, 2), 0) should be(-3)
  }

  "solution 13.0 summation" should "return right surface values" in {
    val coefficients = List(1, 2)
    val powers = List(0, 1)
    val upperLimit = 20
    val lowerLimit = 1
    Solution13.f(coefficients, powers, upperLimit) should be(41)
    Solution13.summation(Solution13.f, upperLimit, lowerLimit, coefficients, powers) should be(414.0 +- (414 * 0.02))
    Solution13.summation(Solution13.area, upperLimit, lowerLimit, coefficients, powers) should be(36024.1 +- (36024.1 * 0.02))
  }

  "solution 13.1 summation" should "return right surface values" in {
    val coefficients = List(1, 2, 3, 4)
    val powers = List(0, 1, 2, 3)
    val upperLimit = 10
    val lowerLimit = 1
    Solution13.f(coefficients, powers, upperLimit) should be(4321)
    Solution13.summation(Solution13.f, upperLimit, lowerLimit, coefficients, powers) should be(11108.2 +- (11108.2 * 0.02))
    Solution13.summation(Solution13.area, upperLimit, lowerLimit, coefficients, powers) should be(86142470.4 +- (86142470.4 * 0.02))
  }

  "solution 13.2 summation" should "return right surface values" in {
    val coefficients = List(1, 2, 3, 4, 5, 6, 7, 8)
    val powers = List(-1, -2, -3, -4, 1, 2, 3, 4)
    val upperLimit = 2
    val lowerLimit = 1
    Solution13.f(coefficients, powers, upperLimit) should be(219.625)
    Solution13.summation(Solution13.f, upperLimit, lowerLimit, coefficients, powers) should be(101.4 +- (101.4 * 0.02))
    Solution13.summation(Solution13.area, upperLimit, lowerLimit, coefficients, powers) should be(41193.0 +- (41193.0 * 0.02))
  }
  "solution 13.3 summation" should "return right surface values" in {
    val coefficients = List(-1, 2, 0, 2, -1, -3, -4, -1, -3, -4, -999, 1, 2, 3, 4, 5)
    val powers = List(-15, -14, -13, -12, -11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0)
    val upperLimit = 10
    val lowerLimit = 1
    Solution13.f(coefficients, powers, upperLimit) should be(5.422105685692019)
    Solution13.summation(Solution13.f, upperLimit, lowerLimit, coefficients, powers) should be(-193.1 +- (193.1 * 0.02))
    Solution13.summation(Solution13.area, upperLimit, lowerLimit, coefficients, powers) should be(336642.8 +- (336642.8 * 0.02))
  }
  "solution 13.4 summation" should "return right surface values" in {
    val coefficients = List(-1, 2, 0, 2, -1, -3, -4, -1, -3, -4, -999, 1, 2, 3, 4, 5, 1, 2, 0, 2, -1, -3, -4, -1, -3, -4, -999, 1, 2, 3, 4, 5)
    val powers = List(-16, -15, -14, -13, -12, -11, -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15)
    val upperLimit = 2
    val lowerLimit = 1
    Solution13.f(coefficients, powers, upperLimit) should be(-762086.6283721924)
    Solution13.summation(Solution13.f, upperLimit, lowerLimit, coefficients, powers) should be(-152853.7 +- (152853.7 * 0.02))
    Solution13.summation(Solution13.area, upperLimit, lowerLimit, coefficients, powers) should be(196838966733.0 +- (196838966733.0 * 0.02))
  }
}
