import org.scalatest.{FlatSpec, Matchers}
import Solution21._
/**
  * Created by rsladek on 23.04.2016.
  */
class Solution21$Test extends FlatSpec with Matchers {

  behavior of "mingling"

  it should "mingling" in {
    mingling(List("Rafal","Natal")) should be("RNaaftaall")
  }

  it should "help" in {
    help("abcde".toList, "pqrst".toList) should be(List('a','p','b','q','c','r','d','s','e','t'))
  }

}
