import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by rsladek on 23.04.2016.
  */
class Solution17$Test extends FlatSpec with Matchers {

  behavior of "rotationOfString"

  it should "concatenate list of characters to a string and print it out" in {
    Solution17.printAWord(List('a', 'b', 'c')) should be("abc")
  }

  it should "rotate string N times" in {
    Solution17.rotationOfString("abc") should be(List("bca", "cab", "abc"))
  }

}
