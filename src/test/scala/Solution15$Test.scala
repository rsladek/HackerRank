import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by rsladek on 23.04.2016.
  */
class Solution15$Test extends FlatSpec with Matchers{

  "15.1 areaOfPolygon" should "return area of 1" in {
    Solution15.areaOfPolygon(List((0, 0), (0, 1), (1, 1), (1, 0))) should be(1)
  }
  "15.2 areaOfPolygon" should "return area of 115342.0" in {
    Solution15.areaOfPolygon(List((1043, 770), (551, 990), (681, 463))) should be(115342.0)
  }
  "15.3 areaOfPolygon" should "return area of 4" in {
    Solution15.areaOfPolygon(List((0, 0), (0, 2), (2, 2), (2, 0))) should be(4)
  }
}
