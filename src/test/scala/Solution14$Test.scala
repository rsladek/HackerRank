import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by rsladek on 23.04.2016.
  */
class Solution14$Test extends FlatSpec with Matchers{

  "14.1 perimeterOfPolygon" should "return 4 as perimeter of the given polygon" in {
    Solution14.perimeterOfPolygon(List((0, 0), (0, 1), (1, 1), (1, 0))) should be(4)
  }

  "14.2 perimeterOfPolygon" should "return perimeter of the given polygon" in {
    Solution14.perimeterOfPolygon(List((1043, 770), (551, 990), (681, 463))) should be(1556.395)
  }

}
