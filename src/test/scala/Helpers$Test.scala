import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by rsladek on 20.04.2016.
  */
class Helpers$Test extends FlatSpec with Matchers {

  behavior of "HelpersSpec"

  it should "getNElementsFromInputList" in {
    Helpers.getNElementsFromInputList(List("2", "3", "4"))(2) should be(List("2", "3"))
  }

  it should "getSumOfAList" in {
    Helpers.getSumOfAList(List(1, 2)) should be(3)
  }

  it should "convertListOfStringsToListOfInts" in {
    Helpers.convertListOfStringsToListOfInts(List("1", "3")) should be(List(1, 3))
  }

  "factorialRecursive" should "return factorial of a give number " in {
    Helpers.factorialRecursive(5) should be(120)
    Helpers.factorialRecursive(10) should be(3628800)
    Helpers.factorialRecursive(20) should be(2432902008176640000L)
  }

  "factorialFP" should "return factorial of a give number " in {
    Helpers.factorialFP(5) should be(120)
    Helpers.factorialFP(10) should be(3628800)
    Helpers.factorialFP(20) should be(2432902008176640000L)
  }

  "generate points by interval" should "return 2 points between 1 and 2" in {
    Helpers.generatePointsWithinLimitByInterval(1d, 2d, 1d) should be(List(1d, 2d))
  }

  "generate points by interval" should "return 3 points between 1 and 2" in {
    Helpers.generatePointsWithinLimitByInterval(1d, 2d, 0.5d) should be(List(1d, 1.5d, 2d))
  }

  "generate points by interval" should "return 3 points between 1.000 and 1.002" in {
    Helpers.generatePointsWithinLimitByInterval(1.000d, 1.002d, 0.001d) should be(List(1.000d, 1.001d, 1.002d))
  }

  "create double with precision" should "return double with 3 scale" in {
    Helpers.doubleToBD32(1.74, 3) should be(1.740)
  }
  "create double with precision" should "return double with 4 scale" in {
    Helpers.doubleToBD32(23.723, 2) should be(23.72)
  }
}